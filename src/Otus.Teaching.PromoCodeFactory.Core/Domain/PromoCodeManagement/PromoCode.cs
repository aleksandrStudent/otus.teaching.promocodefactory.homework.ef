﻿using System;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [StringLength(30)]
        public string Code { get; set; }
        [StringLength(100)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        [StringLength(100)]
        public string PartnerName { get; set; }

        public Employee PartnerManager { get; set; }

        public Preference Preference { get; set; }
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}