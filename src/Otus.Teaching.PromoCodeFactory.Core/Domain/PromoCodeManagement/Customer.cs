﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(250)]
        public string FullName => $"{FirstName} {LastName}";
        [StringLength(50)]
        public string Email { get; set; }

        public IEnumerable<Preference> Preferences {  get; set; }
        public IEnumerable<PromoCode> PromoCodes {  get; set; }

    }
}