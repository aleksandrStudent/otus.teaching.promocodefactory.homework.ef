﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IGenericRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(Guid id);
        IEnumerable<T> Get(Func<T, bool> predicate);
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);
        public Task<IEnumerable<T>> GetInclude(string property);
        Task<T> GetIncludeByIds(string property, List<Guid> ids);
        Task AddAsync(T entity);
        Task AddRangeAsync(List<T> entities);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task<IEnumerable<T>> FindAsync<T1>(Expression<Func<T, bool>> predicate);
        public IEnumerable<T> Find(Func<T, bool> predicate);
        public IEnumerable<T> GetWithInclude(Func<T, bool> predicate,
                 params Expression<Func<T, object>>[] includeProperties);
        public void Test(T entity, T preFunc, Func<T, bool> predicate);
        public Task<IEnumerable<T>> GetWhereAsync(Expression<Func<T, bool>> predicate);
    }
}