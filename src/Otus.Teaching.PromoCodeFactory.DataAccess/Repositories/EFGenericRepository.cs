﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EFGenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        DataContext _dataContext;
        DbSet<T> _dbSet;

        public EFGenericRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<T>();
        }

        public async Task AddAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }
        public async Task AddRangeAsync(List<T> entities)
        {
            await _dbSet.AddRangeAsync(entities);
            await _dataContext.SaveChangesAsync();
        }

        public IEnumerable<T> Get(Func<T, bool> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate).ToList();
        }

        public async Task DeleteAsync(T entity)
        {
            _dbSet.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public IQueryable<T> GetQuery<T1>(Expression<Func<T, bool>> predicate = null) where T1 : class
        {
            IQueryable<T> qry = (IQueryable<T>)_dbSet;
            if (predicate != null)
            {
                qry = qry.Where(predicate);
            }
            return qry;
        }
        public async Task<IEnumerable<T>> FindAsync<T1>(Expression<Func<T, bool>> predicate)
        {
            return await GetQuery<T>(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbSet.FirstAsync(x => x.Id == id);
        }
        public async Task<IEnumerable<T>> GetInclude(string property)
        {
            return await _dbSet.Include(property).ToListAsync();

        }
        public IEnumerable<T> GetWithInclude(Func<T, bool> predicate,
                 params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return query.Where(predicate).ToList();
        }
        private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbSet.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public async Task<T> GetIncludeByIds(string property, List<Guid> ids)
        {
           return await _dbSet.Where(x => ids.Contains(x.Id)).Include(property).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            if (ids == null) return null;
            var entities = await _dbSet.Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task UpdateAsync(T entity)
        {
            await _dataContext.SaveChangesAsync();
        }

        public void Test(T entity, T preFunc,Func<T, bool> predicate)
        {
            
            var customers = _dbSet.Include("Preferences").Where(predicate).FirstOrDefault();
        }
        public async Task<IEnumerable<T>> GetWhereAsync(Expression<Func<T, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var entities = await _dbSet.Where(predicate).ToListAsync();
            return entities;
        }

    }
}
