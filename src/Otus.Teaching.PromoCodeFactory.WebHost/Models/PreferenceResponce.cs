﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponce
    {
        public string Name { get; set; }
        public List<CustomerResponse> CustomerResponses { get; set; }
    }
}
