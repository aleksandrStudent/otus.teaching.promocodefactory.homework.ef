﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Mapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IGenericRepository<Customer> _customerRepository;
        private IGenericRepository<Preference> _preferenceRepository;
        private IGenericRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IGenericRepository<Customer> customerRepository,
            IGenericRepository<Preference> preferenceRepository, IGenericRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Get customers list
        /// </summary>
        /// <remarks>
        /// Получение списка клиентов
        /// </remarks>
        [HttpGet]
        public async Task<List<CustomerResponse>> GetCustomersAsync()
        {
            IEnumerable<Customer> customers = (IEnumerable<Customer>)await _customerRepository.GetInclude("Preferences");

            List<CustomerResponse> customerResponses = new();
            List<PromoCode> promoCodes = (List<PromoCode>)await _promoCodeRepository.GetAllAsync(); 

            foreach (Customer c in customers)
            {
                List<PreferenceResponce> preferences = new();
                foreach (Preference preference in c.Preferences)
                {
                    preferences.Add(new PreferenceResponce()
                    {
                        Name = preference.Name,
                    });
                }

                List<PromoCodeShortResponse> promoCodeShortResponses = new();

                var promo = promoCodes.Where(x => x.CustomerId == c.Id).ToList();
                foreach (PromoCode promoCode in promo)
                {
                    promoCodeShortResponses.Add(new PromoCodeShortResponse()
                    {
                        BeginDate = promoCode.BeginDate.ToString(),
                        EndDate = promoCode.EndDate.ToString(),
                        Code = promoCode.Code,
                        Id = promoCode.Id,
                        PartnerName = promoCode.PartnerName,
                        ServiceInfo = promoCode.ServiceInfo
                    });
                }

                customerResponses.Add(new CustomerResponse()
                {
                    Id = c.Id,
                    LastName = c.LastName,
                    Email = c.Email,
                    FirstName = c.FirstName,
                    PreferenceResponces = preferences,
                    PromoCodes = promoCodeShortResponses
                });
            }
            return customerResponses;
        }

        /// <summary>
        /// Get customer by id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// Получение клиента вместе с выданными ему промомкодами
        /// </remarks>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            Customer customer = (Customer)await _customerRepository.GetByIdAsync(id);

            List<PromoCodeShortResponse> promoCodeShortResponses = new();
            foreach (PromoCode promoCode in customer.PromoCodes)
            {
                promoCodeShortResponses.Add(new PromoCodeShortResponse()
                {
                    BeginDate = promoCode.BeginDate.ToString(),
                    EndDate = promoCode.EndDate.ToString(),
                    Code= promoCode.Code,
                    Id= promoCode.Id,
                    PartnerName = promoCode.PartnerName,
                    ServiceInfo = promoCode.ServiceInfo
                });
            }
            CustomerResponse customerShortResponse = new()
            {
                Id = customer.Id,
                LastName = customer.LastName,
                Email = customer.Email,
                FirstName = customer.FirstName,
                PromoCodes = promoCodeShortResponses
            };
            return customerShortResponse;
        }

        /// <summary>
        /// Create customer
        /// </summary>
        /// <param name="request"></param>
        /// <remarks>
        /// Cоздание нового клиента вместе с его предпочтениями
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, customer.Id);
        }

        /// <summary>
        /// Edit customer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <remarks>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </remarks>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = (Customer)await _customerRepository.GetIncludeByIds("Preferences", new List<Guid>() { id });
            if (customer == null)
                return NotFound();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            
            CustomerMapper.MapFromModel(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);
            
            return NoContent();
        }

        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// Удалить клиента вместе с выданными ему промокодами
        /// </remarks>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            await _customerRepository.DeleteAsync(customer);
            return NoContent();
        }
    }
}