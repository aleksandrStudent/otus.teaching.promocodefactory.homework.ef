﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private IGenericRepository<Preference> _preferenceRepository;

        public PreferencesController(IGenericRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Get preferences list
        /// </summary>
        /// <remarks>
        /// Получение списка предпочтений
        /// </remarks>
        public async Task<List<PreferenceResponce>> GetPreferencesAsync()
        {
            
            IEnumerable<Preference> preferences = (IEnumerable<Preference>)await _preferenceRepository.GetInclude("Customers");
            List<PreferenceResponce> preferenceResponces = new();
            
            foreach (Preference p in preferences)
            {
                List<CustomerResponse> customerResponses = new();

                foreach (Customer customer in p.Customers)
                {
                    customerResponses.Add(new CustomerResponse()
                    {
                        Email= customer.Email,
                        FirstName= customer.FirstName,
                        LastName= customer.LastName,
                        Id= customer.Id,
                        PromoCodes= (List<PromoCodeShortResponse>)customer.PromoCodes
                    });
                }

                preferenceResponces.Add(new PreferenceResponce()
                {
                    CustomerResponses = customerResponses,
                    Name = p.Name
                });

            }

            return preferenceResponces;
        }



    }
}
