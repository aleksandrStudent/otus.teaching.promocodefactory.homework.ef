﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {

        private IGenericRepository<PromoCode> _promoCodeRepository;
        private IGenericRepository<Preference> _preferencesRepository;
        private IGenericRepository<Customer> _customerRepository;

        public PromoCodesController(IGenericRepository<PromoCode> promoCodeRepository, IGenericRepository<Preference> preferencesRepository
            , IGenericRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferencesRepository = preferencesRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {

            var promoCodes = await _promoCodeRepository.GetAllAsync();
            if (promoCodes == null)
                return NotFound();


            var shortResponses = promoCodes.Select(p => new PromoCodeShortResponse()
            {
                Id = p.Id,
                Code = p.Code,
                BeginDate = p.BeginDate.ToString(),
                EndDate = p.EndDate.ToString(),
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            }).ToList();

            return Ok(shortResponses);

        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var listPreference = await _preferencesRepository.GetWhereAsync(
              x => x.Name == request.Preference);
            if (listPreference == null)
                return BadRequest("Не найдено предпочтение");
            
            Preference preference =  listPreference.FirstOrDefault();

            var customers = await _customerRepository
                .GetWhereAsync(c => c.Preferences.Any(x =>
                    x.Id == preference.Id));

            List<PromoCode> promoCodes = new();
            foreach(var c in customers)
            {
                PromoCode promo = new()
                {
                    Code = Guid.NewGuid().ToString(),
                    CustomerId = c.Id,
                    Preference = preference,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName
                };
                promoCodes.Add(promo);
            }


            await _promoCodeRepository.AddRangeAsync(promoCodes);

            return Ok();
        
        }
    }
}